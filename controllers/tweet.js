const { pool } = require('../config/db');
const { twitterClient } = require('../config/twitter');
const openai = require('../config/openai');

const { prompt, wildcards } = require('../utils/prompts');

module.exports = async (req, res) => {

    try {

        const { refresh_token: refreshToken } = (await pool.query('SELECT refresh_token FROM twitter_auth WHERE id = 1')).rows[0];


        const { client: refreshedClient, accessToken, refreshToken: newRefreshToken } = (await twitterClient.refreshOAuth2Token(refreshToken));

        await pool.query('UPDATE twitter_auth SET access_token = $1, refresh_token = $2 WHERE id = 1', [ accessToken, newRefreshToken ]);

        const me = await refreshedClient.v2.me();

        const promptNumber = Math.floor(Math.random() * prompt.length);
        const wildcardsNumber = Math.floor(Math.random() * wildcards.length);
        const textToComplete = `${prompt[promptNumber]} ${wildcards[wildcardsNumber]}`;

        const response = await openai.createCompletion('text-davinci-001', {
            prompt: textToComplete,
            max_tokens: 48
            
        });

        const { data: createdTweet } = await refreshedClient.v2.tweet(response.data.choices[0].text);
        
        res.status(200).json({
            status: 'success',
            data: {
                me,
                createdTweet
            }
        });

    } catch (err) {

        console.log('Error', err);

    }

};