const { pool } = require('../config/db');
const { twitterClient } = require('../config/twitter');
const axios = require('axios');
const { CronJob } = require('cron');



module.exports = async (req, res) => {

    try {

        const { state: queryState, code: queryCode } = req.query;

        const { code_verifier: codeVerifier, state } = (await pool.query('SELECT code_verifier, state FROM twitter_auth WHERE id = 1')).rows[0];

        if (!codeVerifier || !state || !queryState || !queryCode) {
            return res.status(400).json({
                status: 'fail',
                data: {
                    message: 'You denied the app or your session expired!'
                }
            });
        }

        if (queryState !== state) {
            return res.status(400).json({
                status: 'fail',
                data: {
                    message: 'Stored tokens do not match!'
                }
            })
        }

        const callbackURL = 'http://127.0.0.1:3000/callback';

        const { client: loggedClient, accessToken, refreshToken, expiresIn} = await twitterClient.loginWithOAuth2({
            code: queryCode,
            codeVerifier,
            redirectUri: callbackURL
        });

        await pool.query('UPDATE twitter_auth SET access_token = $1, refresh_token = $2 WHERE id = 1', [ accessToken, refreshToken ]);

        const data = await loggedClient.v2.me();

        const everyMinutes = '0 1 * * * *';

        const job = new CronJob(everyMinutes, async () => {

            try {
                await axios.get('http://127.0.0.1:3000/tweet');
                console.log('New tweet');
            } catch (err) {
                console.error('erreur cron');
            }
        }, null, true, 'Europe/Paris');
        job.start();

        res.status(200).json({
            status: 'success',
            data
        });        

    } catch (err) {

        console.log(err);

    }

};