const { pool } = require('../config/db');
const { twitterClient } = require('../config/twitter');

module.exports = async (req, res) => {

    try {

        const callbackURL = 'http://127.0.0.1:3000/callback';
        
        const link = twitterClient.generateOAuth2AuthLink(
            callbackURL,
            { scope: ['tweet.read', 'tweet.write', 'users.read', 'offline.access'] }
        );

        const { url, codeVerifier, state } = link;

        await pool.query('UPDATE twitter_auth SET code_verifier = $1, state = $2 WHERE id = 1', [ codeVerifier, state ]);
        
        res.redirect(302, url);

    } catch (err) {

        console.error('Error', err);

    }
};