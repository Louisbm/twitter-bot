const dotenv = require('dotenv');
const express = require('express');

dotenv.config({ path: './config.env' });

const authentication = require('./controllers/authentication');
const callback = require('./controllers/callback');
const tweet = require('./controllers/tweet');

const app = express();

app.get('/auth', authentication);
app.get('/callback', callback);
app.get('/tweet', tweet);

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});